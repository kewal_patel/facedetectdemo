from django.apps import AppConfig


class FacetestConfig(AppConfig):
    name = 'facetest'
