import os
import json
import base64
import cv2
import numpy as np
from StringIO import StringIO
from PIL import Image
from django.core.files.base import ContentFile
from django.conf import settings
from django.shortcuts import render, render_to_response
from django.views.generic import TemplateView, View
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt


# Create your views here.
class HomePageView(TemplateView):
    template_name = 'home_page.html'


def base642mat(img_string):
    """Decodes Base64 string to an image array"""
    first_coma = img_string.find(',')
    img_bytes = base64.decodestring(img_string[first_coma:])
    image = np.asarray(bytearray(img_bytes), dtype="uint8")
    image = cv2.imdecode(image, cv2.IMREAD_COLOR)
    return image


class SavePictureView(View):
    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        return super(SavePictureView, self).dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        if request.FILES:
            print request.FILES
            # img_str = base64.b64encode(request.FILES['fileToUpload'].read())
            uploaded_filename = request.FILES['fileToUpload'].name
            # full_filename = os.path.join('/',uploaded_filename)
            # fout = open(uploaded_filename, 'wb+')
            file_path = os.path.join(settings.BASE_DIR, uploaded_filename)
            print file_path
            if os.path.exists(file_path):
                print "file exists"
                os.remove(file_path)
            else:
                print "does not exist"
            with open(uploaded_filename, 'wb+') as fout:
                file_content = ContentFile(request.FILES['fileToUpload'].read())
                for chunk in file_content.chunks():
                    fout.write(chunk)
                    fout.close()
            img = cv2.imread(file_path)
            # print img, 'files'
        else:
            img_str = json.loads(request.body)
            img = base642mat(img_str['image'])

        print img,"image"
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        face_cascade = cv2.CascadeClassifier('/usr/share/opencv/haarcascades/haarcascade_frontalface_default.xml')
        eye_cascade = cv2.CascadeClassifier('/usr/share/opencv/haarcascades/haarcascade_eye.xml')
        lips_cascade = cv2.CascadeClassifier('haar_cascade_files/lips.xml')
        faces = face_cascade.detectMultiScale(gray, 1.3, 5)

        for (x, y, w, h) in faces:
            cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 0), 2)
            roi_gray = gray[y:y + h, x:x + w]
            roi_color = img[y:y + h, x:x + w]
            eyes = eye_cascade.detectMultiScale(roi_gray)
            for (ex, ey, ew, eh) in eyes:
                cv2.rectangle(roi_color, (ex, ey), (ex + ew, ey + eh), (0, 255, 0), 2)
            lips = lips_cascade.detectMultiScale(roi_gray)
            # print(lips[-1])
            # for (ex, ey, ew, eh) in lips:
            (ex, ey, ew, eh) = lips[0]
            cv2.rectangle(roi_color, (ex, ey), (ex + ew, ey + eh), (0, 255, 0), 2)
        
        detected_file_path = os.path.join(settings.BASE_DIR,'detected_img.jpg')
        if os.path.exists(detected_file_path):
            os.remove(detected_file_path)

        cv2.imwrite(detected_file_path, img)
        return HttpResponse('')
