$(document).ready(function(){
var video = document.getElementById('video');
var canvas = document.getElementById('canvas');


// Get access to the camera!
if(navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
    // Not adding `{ audio: true }` since we only want video now
    navigator.mediaDevices.getUserMedia({ video: true }).then(function(stream) {
        video.src = window.URL.createObjectURL(stream);
        video.play();
    });
}

/* Legacy code below: getUserMedia 
else if(navigator.getUserMedia) { // Standard
    navigator.getUserMedia({ video: true }, function(stream) {
        video.src = stream;
        video.play();
    }, errBack);
} else if(navigator.webkitGetUserMedia) { // WebKit-prefixed
    navigator.webkitGetUserMedia({ video: true }, function(stream){
        video.src = window.webkitURL.createObjectURL(stream);
        video.play();
    }, errBack);
} else if(navigator.mozGetUserMedia) { // Mozilla-prefixed
    navigator.mozGetUserMedia({ video: true }, function(stream){
        video.src = window.URL.createObjectURL(stream);
        video.play();
    }, errBack);
}
*/

    $('#snap').click(function(){
        
        canvas.height = video.videoHeight
        canvas.width = video.videoWidth
        canvas.getContext('2d').drawImage(video, 0, 0);   

        var imgData = canvas.toDataURL("img/png")
        // imgData = imgData.replace('data:image/png;base64,','')
        // var postData = JSON.stringify({'imageData':imgData})
        $.ajax({
            url:"/save/",
            type: "POST",
            data: JSON.stringify({ 'image' : imgData}),
//            contentType: 'application/json'
        });
    });

//     $('#upload_img').click(function(e){
//         e.preventDefault()
//         img = $('#fileToUpload')
//         canvas.getContext('2d').drawImage(img,0,0)
//         var imgData = canvas.toDataURL("img/png")
//         console.log(imgData)
//         // imgData = imgData.replace('data:image/png;base64,','')
//         // var postData = JSON.stringify({'imageData':imgData})
//         $.ajax({
//             url:"/save/",
//             type: "POST",
//             data: JSON.stringify({ 'image' : imgData}),
// //            contentType: 'application/json'
//         });
//     })


});

// document.getElementById("snap").addEventListener("click", function(){
    // document.getElementById("demo").innerHTML = "Hello World";
    

    // Trigger photo take


// });
